"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("posts", [
      {
        title: "4 Things That Will Vanish In 20 Years",
        excerpt: "Some goodbyes hurt more than others",
        content: "nais ashiap santuy",
        slug: "4-things-that-will-vanish-in-20-years",
        createdAt: "2022-02-15",
        updatedAt: "2022-02-15",
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("posts", null, {});
  },
};
