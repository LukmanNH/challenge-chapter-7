"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
    static #encrypt = (password) => bcrypt.hashSync(password, 10);
    static register = ({ name, username, password, age, userLevel }) => {
      const encryptedPassword = this.#encrypt(password);
      return this.create({
        name,
        username,
        password: encryptedPassword,
        age,
        userLevel,
      });
    };
    checkPassword = (password) => bcrypt.compareSync(password, this.password);
    generateToken = () => {
      const payload = {
        id: this.id,
        name: this.name,
        username: this.username,
        age: this.age,
        userLevel: this.userLevel,
      };
      const salt = "jangan bilang siapa2";
      const token = jwt.sign(payload, salt);
      return token;
    };

    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        if (!user) return Promise.reject("User not Found");
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Wrong Password");
        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }
  user.init(
    {
      name: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      age: DataTypes.INTEGER,
      userLevel: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "user",
      timestamps: false,
    }
  );
  return user;
};
