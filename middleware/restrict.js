const passport = require("../lib/passport");
const jwt = require("jsonwebtoken");

module.exports = {
  loggedUser: passport.authenticate("jwt", {
    session: false,
  }),
  superUser: async (req, res, next) => {
    try {
      const tokenJWT = req.headers.authorization;
      let decodeToken = await jwt.verify(tokenJWT, "jangan bilang siapa2");
      if (decodeToken.userLevel !== 1) {
        return res.status(401).json({ status: 401, msg: "UNAUTHORIZED" });
      }
      next();
    } catch (err) {
      res
        .status(501)
        .json({ problem: err.message, msg: "Mohon Login terlebih dahulu!" });
    }
  },
};
