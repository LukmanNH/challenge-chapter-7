const router = require("express").Router();
const authController = require("./controller/authController");
const userController = require("./controller/userController");
const postController = require("./controller/postController");
const restrict = require("./middleware/restrict");

// AUTH ROUTER
router.post("/api/v1/auth/login", authController.login);
router.post(
  "/api/v1/auth/register",
  restrict.loggedUser,
  authController.register
);

// USER ROUTER
router.get("/api/v1/user", restrict.superUser, userController.showAllUser);
router.get("/api/v1/user/:id", restrict.loggedUser, userController.profile);

// POST ROUTER
router.get("/api/v1/post", postController.showAllPost);
router.get("/api/v1/post/:id", postController.detailPost);
router.post("/api/v1/post", restrict.loggedUser, postController.addPost);
router.delete(
  "/api/v1/post/:id",
  restrict.superUser,
  postController.deletePost
);

module.exports = router;
