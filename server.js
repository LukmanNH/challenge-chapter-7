const express = require("express");
const app = express();

app.use(express.json());

const PORT = 5001;

const passport = require("./lib/passport");
app.use(passport.initialize());

const router = require("./router");
app.use(router);
app.listen(PORT, () => console.log("server listen to port" + PORT));
