const { user } = require("../models");

module.exports = {
  showAllUser: async (req, res) => {
    const getAllUser = await user.findAll();
    res.json({ status: 200, msg: "OK", data: getAllUser });
  },
  profile: async (req, res) => {
    const userId = req.params.id;
    const getSpesificUser = await user.findByPk(userId);
    res.json({ status: 200, msg: "OK", data: getSpesificUser });
  },
};
