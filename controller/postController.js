const { post } = require("../models");

module.exports = {
  showAllPost: async (req, res, next) => {
    const getAllPosts = await post.findAll();
    res.json({ status: 200, msg: "OK", data: getAllPosts });
  },
  deletePost: async (req, res, next) => {
    const idPost = req.params.id;
    // const getPostById = await post.findByPk(idPost);
    post.destroy({
      where: {
        id: idPost,
      },
    });
    res.json({ status: 200, msg: "Data Berhasil Dihapus" });
  },
  detailPost: async (req, res, next) => {
    const idPost = req.params.id;
    const getPostById = await post.findByPk(idPost);
    res.json({ status: 200, msg: "OK", data: getPostById });
  },
  addPost: async (req, res, next) => {
    const dateObj = new Date();
    const month = dateObj.getMonth();
    const day = String(dateObj.getDate()).padStart(2, "0");
    const year = dateObj.getFullYear();
    const fullDate = year + "-" + month + "-" + day;
    const title = req.body.title;
    const excerpt = req.body.excerpt;
    const content = req.body.content;
    const slug = req.body.slug;
    const createdAt = req.body.createdAt || fullDate;
    const updatedAt = req.body.updatedAt || fullDate;

    await post.create({
      title,
      excerpt,
      content,
      slug,
      createdAt,
      updatedAt,
    });
    res.status(201).json({ status: 201, msg: "Success Create Post" });
  },
};
