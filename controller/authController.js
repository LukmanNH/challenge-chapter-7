const { user } = require("../models");

function format(user) {
  const { id, username, userLevel } = user;
  return {
    id,
    username,
    userLevel,
    accessToken: user.generateToken(),
  };
}

module.exports = {
  register: async (req, res, next) => {
    user
      .register(req.body)
      .then(() => {
        res.json({ msg: "Register Berhasil" });
      })
      .catch((err) => next(err));
  },
  login: (req, res) => {
    user.authenticate(req.body).then((user) => {
      res.json(format(user));
    });
  },
};
